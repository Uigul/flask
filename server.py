from flask import Flask, jsonify, request
from dao import Dao
import json
app = Flask(__name__)

current_active = 1

@app.route("/configuration-list/all")
def configuration_list():
    dao = Dao()
    response = []
    for i in dao.get_all_lists():
        response.append(json.loads(i.__repr__()))
    return jsonify(response)


@app.route("/configuration-list/details/<id>")
def single_configuration(id):
    dao = Dao()
    response = json.loads(dao.get_list_by_id(id).first().__repr__())
    return jsonify(response)


@app.route("/configuration-list/sensors/<id>")
def single_configuration_sensors_by_id(id):
    dao = Dao()
    response = json.loads(dao.get_sensor_by_id(id).__repr__())
    return jsonify(response)


@app.route("/configuration-list/new", methods=['POST'])
def create_configutration():
    print(request.json)
    data = request.json
    dao = Dao()
    dao.create_list(data['name'])
    id = (str(dao.get_list_id(data['name'])))
    return id


@app.route("/configuration-list/delete/<id>", methods=['DELETE'])
def delete_configutration(id):
    dao = Dao()
    print(type(id))
    dao.delete_list(id)
    return "DELETED"


@app.route("/configuration-list/sensors/<id>/<sensor>", methods=['PATCH'])
def update_single_sensor(id, sensor):
    data = request.json
    dao = Dao()
    dao.update_sensor(id, sensor, kp=data['values']['Kp'], ki=data['values']['Ki'],
                      kd=data['values']['Kd'], tf=data['values']['Tf'])
    return "UPDATED"


@app.route("/configuration-list/sensors/current/<id>", methods=['POST'])
def set_active_configuration_list(id):
    global current_active
    current_active = id
    return current_active

@app.route("/configuration-list/sensors/current", methods=['GET'])
def get_active_configuration_list():
    dao = Dao()
    response = json.loads(dao.get_sensor_by_id(current_active).__repr__())
    return jsonify(response)
